#!/bin/bash

dir_folder () {
    jam=$(date +"%H")

    if (( $jam >= 0 && $jam < 2 )); then
      jumlah_gambar=1
    else
      jumlah_gambar=$((jam-1))
    fi

    nomor_folder=$(ls -1d kumpulan_* 2>/dev/null | wc -l)
    let nomor_folder++
    nama_folder="kumpulan_$nomor_folder"
    mkdir "$nama_folder"

    for ((i=1;i<=jumlah_gambar;i++)); do
      nomor_file=$(ls -1 $nama_folder/perjalanan_* 2>/dev/null | wc -l)
      let nomor_file++
      wget -q "https://source.unsplash.com/1600x900/?Indonesia" -O "$nama_folder/perjalanan_$nomor_file.jpg"
    done
}

# Function to zip the folders
zip_folder () {
  nomor_zip=$(ls -1d devil_* 2>/dev/null | wc -l)
  ((nomor_zip++))

  zip_file="devil_$nomor_zip.zip"

  for dir in kumpulan_*/; do
    dir=${dir%/}
    if [[ $dir = *"kumpulan_"* ]]; then
      zip -qr "$zip_file" "$dir"
    fi
  done
}

if [ "$1" == "dir_folder" ]; then
    dir_folder
elif [ "$1" == "zip_folder" ]; then
    zip_folder
else
    echo "Invalid argument"
    exit 1
fi

# crontab -e
# 0 */6 * * * ~/kobeni_liburan.sh dir_folder
# 0 0 * * * ~/kobeni_liburan.sh zip_folder