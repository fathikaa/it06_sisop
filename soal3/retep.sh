#!/bin/bash

echo "Selamat datang di sistem login"
printf "Masukkan username: " 
read -r username
printf "Masukkan password: "
read -rs password

# Cek apakah user sudah terdaftar
if ! grep -q "^$username:" /home/kali/soal3/users/users.txt; then
    echo "User tidak terdaftar"
    echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Cek apakah password benar
saved_password=$(grep "^$username:" /home/kali/soal3/users/users.txt | cut -d: -f2)
if [[ "$password" != "$saved_password" ]]; then
    echo "Password salah"
    echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Jika berhasil login
echo "Selamat datang $username"
echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> log.txt
