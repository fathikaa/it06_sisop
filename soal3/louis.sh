#!/bin/bash

echo "Selamat datang di sistem register"
printf "Masukkan username: " 
read -r username
printf "Masukkan password: "
read -rs password

# Validasi password
if [[ ${#password} -lt 8 ]] || ! [[ "$password" =~ [a-z] ]] || ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [0-9] ]] || [[ "$password" == *"$username"* ]] || [[ "$password" =~ "chicken" ]] || [[ "$password" =~ "ernie" ]]; then
    echo "Password tidak memenuhi kriteria yang ditentukan"
    exit 1
fi

# Cek apakah user sudah terdaftar
if grep -q "^$username:" /home/kali/soal3/users/users.txt; then
    echo "User sudah terdaftar"
    echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User $username already exists" >> log.txt
    exit 1
fi

# Menambahkan user baru ke dalam file users.txt
echo "$username:$password" >> /home/kali/soal3/users/users.txt
echo "User $username berhasil didaftarkan"
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> log.txt
